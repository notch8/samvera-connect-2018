FROM ruby:2.5.1
RUN echo 'Downloading Packages' && \
    apt-get update -qq && \
    apt-get install -y apt-transport-https && \
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get update -qq && \
    apt-get install -y build-essential nodejs yarn pv libpq-dev libsasl2-dev mysql-client zip unzip imagemagick tzdata vim sqlite3 libsqlite3-dev && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN gem install brakeman bundler-audit
RUN npm install -g retire

ENV APP_DIR /app
RUN mkdir -p $APP_DIR/api && mkdir -p $APP_DIR/samvera-react

WORKDIR $APP_DIR/api

ADD api/Gemfile $APP_DIR/api/Gemfile
ADD api/Gemfile.lock $APP_DIR/api/Gemfile.lock
ENV BUNDLE_JOBS=4
RUN bundle install

ADD samvera-react/Gemfile $APP_DIR/samvera-react/Gemfile
ADD samvera-react/Gemfile.lock $APP_DIR/samvera-react/Gemfile.lock
ENV BUNDLE_JOBS=4
RUN bundle install


ADD . /app 

EXPOSE 3000
EXPOSE 3001
ADD bashrc /root/.bashrc
CMD ["/bin/bash", "-l"]