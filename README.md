# Base repo for Notch8 Exercises Samvera Connect 2018

## Install Docker on your machine

You want docker CE, any relatively recent version should work for our purposes

https://docs.docker.com/install/

## Pull this gitlab repo on to your machine

https://gitlab.com/notch8/samvera-connect-2018

## Pull the docker image down locally

Run the following in the terminal in the directory you checked out the repo in

`docker-compose pull`

## To start the image

Run the following in the terminal in the directory you checked out the repo in

`docker-compose up web`

To log in to the shell once the contianer is running, in a new terminal (or tab)

`docker-compose exec web bash`

