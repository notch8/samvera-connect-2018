import React, { Component } from 'react'

class PublicExample extends Component {
    render() {
        return (
            <div>
                This page is public!
                <br />
                <a href="/protected">Check out the secret area</a>
            </div>

        )
    }
}

export default PublicExample
