import React, { Component } from 'react'
import {withRouter} from "react-router-dom";

import AuthService from '../services'
import Header from '../components/Header'

class Login extends Component {
    constructor(props) {
        super(props)

        this.auth = new AuthService()
        this.state = {
            success: false,
            errors: '',
            user: {
                email: "",
                password: ""
            }
        }
    }

    render() {
        let { email, password } = this.state.user
        if(this.state.success) {
            return(
                <main>
                    <Header />
                    <a href='/protected'>Go To Protected</a>
                </main>
            )
        } else {
            return (
            <main>
                <Header />
                { this.state.errors }
                <form onSubmit={this.handleSubmit}>
                    <input
                        type="email"
                        placeholder="Email"
                        name="email"
                        value={email}
                        onChange={this.handleChange}
                    />
                    <input
                        type="password"
                        placeholder="Password"
                        name="password"
                        value={password}
                        onChange={this.handleChange}
                    />
                    <input
                        type="submit"
                        value="Login"
                    />
                </form>
            </main>
            )
        }
    }

    // track each keystroke, save the value to state
    handleChange = (event) => {
        // copy form from state
        let { user } = this.state

        // copy event target name and value (target will be a form field)
        let fieldName = event.target.name
        let inputValue = event.target.value

        console.log(inputValue, fieldName);

        // update form object with new value from user
        user[fieldName] = inputValue

        this.setState({user})
    }

    // when user submits form,
    handleSubmit = (e) => {
        e.preventDefault()
        // this function requires an email and password
        this.auth.login(this.state)
        .then(json => {
			      console.log("handling any errors");
			      if(json.errors) {
				        this.setState({
					          errors: ['failed to log in'] + json.errors
				        })
			      } else {
                this.setState({
                    errors: '',
                    success: true
                })
            }
			      return json
		    })
    }
}

export default withRouter(Login)
