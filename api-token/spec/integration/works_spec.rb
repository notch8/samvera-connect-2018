require 'swagger_helper'

describe 'Works API' do
  before(:all) do
    @user = User.create!(email: 'test@example.com', password: 'testing123')
    Work.create!(title: 'two', description: 'lorem ipsum', user: @user)
    Work.create!(title: 'one', description: 'lorem ipsum', user: @user)
  end
  path '/api/works.json' do
    get 'Read a list of works' do
      tags 'Works'
      produces 'application/json'
      consumes 'application/json', 'application/xml'
      response '200', 'works found' do
        schema type: :array,
               works: {
                 properties: {
                   works: { title: :string },
                 }
               }
        run_test!
      end
    end

    post 'Create a new work' do
      tags 'Works'
      produces 'application/json'
      consumes 'application/json', 'application/xml'
      parameter name: :work, in: :body, schema: {
                  type: :object,
                  properties: {
                    title: { type: :string },
                    description: { type: :text },
                    user_id: { type: :integer }
                  },
                  required: [ 'title', 'description', 'user_id' ]
                }

      response '201', 'work created' do
        let(:work) { { title: 'foo', description: 'bar', user_id: @user.id } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:work) { { title: 'foo' } }
        run_test!
      end
    end
  end
end
